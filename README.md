# x-report

> 基于xspreadsheet二次开发的web版报表插件

> demo  https://hugeman.gitee.io/report/index.html

> very thanks https://github.com/myliang/x-spreadsheet.git

```html
<!-- 第一种用法 -->
<link href="xreport.css" rel="stylesheet"></head>
<div id="x-report-demo"></div>
<script type="text/javascript" src="xreport.js"></script>
<!-- 
  第二种用法 
  把dist放入自己的项目里并改成合适的名字 
  然后import 导入  css js
-->
<div id="x-report-demo"></div>

```

```javascript
// 详细用法 请查看 https://hugeman.gitee.io/report/index.html
//第一种; 
const xreport = window.xreport;
//第二种的 路径根据自己所方位置改变
import './xreport.css';
import xreport from "./xreport.js";

//第一种、第二种 只区其一
let demoData = {
  name: '测试',
  freeze:'A1',
  cols: {len:26},
  rows: {len:50},
  merges: [],//合并单元格的数据
  groups: [],//分组的数据
  styles: [],//单元格的样式
  dataDb: [],//数据的信息与dataSource对应  
  /*
    dataDb: [
          {
            "label":"人员列表",
            "value":"table001",
            "codes":[
              {"label":"名称","value":"name"},
              {"label":"年龄","value":"age"}
            ]
          }
        ]
  */
  dataSource: {},//数据集合，不会被存储
  /*
    dataSource: {
      table001: [{name:'张三',age:'28'},{name:'李四',age:'30'}]
    }
  */
  dataFilter: [],//过滤dataSource信息配置
  autofilter: [],//暂时用不到
  validations: []//暂时用不到
};
const s = xreport("#x-report-demo")
  .loadData(demoData) // 具体参数
  .change(data => {
    // save data to db
  });
  // event of click on cell
  s.on('cell-selected', (cell, ri, ci) => {});
  s.on('cells-selected', (cell, { sri, sci, eri, eci }) => {});
  // edited on cell
  s.on('cell-edited', (text, ri, ci) => {});
  //toolbar 拦截器
  s.on('toolbar-click-before',(type,value)=>{
    //头部按钮拦截；执行reject就不会触发更改操作
    if(type != 'save')return true;
    return new Promise((resolve)=>{
      setTimeout(()=>{
        let nowData = xs.getData();
        window.localStorage.setItem('sheetDemo',JSON.stringify(nowData))
        resolve()
      },10)
    })
  });
```
## Development

```sheel
git clone https://gitee.com/hugeman/x-report.git
cd x-report
npm install
npm run dev
```

Open your browser and visit http://127.0.0.1:8080.

## Browser Support

Modern browsers(chrome, firefox, Safari).

## LICENSE

MIT
