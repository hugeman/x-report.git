import { Element, h } from './element';
import { cssPrefix } from '../config';
import Dropdown from './dropdown';
import ColorPalette from './color_palette';

export default class ColorPick extends Dropdown{
  constructor(color) {
    const iconNameEl = h('input', `${cssPrefix}-colorPick`).attr('readonly',true);
    const colorPalette = new ColorPalette();
    colorPalette.change = (v) => {
      this.setTitle(v);
      this.colorChange && this.colorChange(v);
      this.iconNameEl.val(v);
      this.iconNameEl.trigger('input')
      this.hide();
    };
    iconNameEl.on('click.stop',()=>{
      this.show();
    });
    super(iconNameEl, 'auto', false, 'bottom-left inline-block', colorPalette.el);
    this.iconNameEl = iconNameEl;
    color&&this.setTitle(color)
  }
  setTitle(v){
    this.iconNameEl.css('background',v).val(v);
  }
  model($reactive,model,key){
    this.iconNameEl.model($reactive,model,key)
    return this;
  }
  change(callback){
    this.colorChange = callback;
    return this;
  }
}