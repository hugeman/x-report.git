import DropdownItem from './dropdown_item';
import DropdownExpand from '../dropdown_expand';

export default class Expand extends DropdownItem {
  constructor(value) {
    super('expand', '', value);
  }

  dropdown() {
    const { value } = this;
    return new DropdownExpand(['down', 'right'], value);
  }
}
