import Item from './item';
import Icon from '../icon';

export default class IconItem extends Item {
  element() {
    this.iconEl = new Icon(this.tag);
    return super.element()
      .child(this.iconEl)
      .on('click', () => this.change(this.tag));
  }

  setState(disabled) {
    this.el.disabled(disabled);
  }
}
