import IconItem from './icon_item';

export default class DataCondition extends IconItem {
  constructor() {
    super('dataCondition');
  }
}
