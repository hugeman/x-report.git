import Item from './item';
import Icon from '../icon';

export default class ToggleItem extends Item {
  element() {
    this.iconEl = new Icon(this.tag);
    return super.element()
      .child(this.iconEl)
      .on('click', () => this.click());
  }

  click() {
    this.change(this.tag, this.toggle());
  }

  setState(active=false) {
    this.el.active(active);
  }

  toggle() {
    return this.el.toggle();
  }

  active() {
    return this.el.hasClass('active');
  }
}
