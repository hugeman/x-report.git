import IconItem from './icon_item';

export default class Preview extends IconItem {
  constructor() {
    super('preview');
  }
}
