import ToggleItem from './toggle_item';

export default class Group extends ToggleItem {
  constructor() {
    super('group');
  }

  setState(active, disabled) {
    this.el.active(active).disabled(disabled);
  }
}
