import ToggleItem from './toggle_item';

export default class ListMark extends ToggleItem {
  constructor(value = true) {
    super('listMark','',value);
    this.iconEl.addClass('listMark')
  }
}
