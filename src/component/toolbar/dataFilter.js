import IconItem from './icon_item';

export default class DataFilter extends IconItem {
  constructor() {
    super('dataFilter');
  }
}
