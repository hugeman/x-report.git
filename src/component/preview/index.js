import { h } from '../element';
import { cssPrefix } from '../../config';
import Button from '../button';
import { debounce,isNumber } from '../../core/helper';
import { t } from '../../locale/locale';
import { exportExcel } from './utils'
import { getTable, setText, setMerge, setStyle,setConditionStyle } from './preview'

const btnClick = function (type) {
  if (type === 'close') {
    this.el.hide();
  } else {
    this.export();
  }
};
const addTrDom = function(){
  return h('tr',`${cssPrefix}-preview-tr`);
},
addTdDom = function(){
  return h('td',`${cssPrefix}-preview-td`);
},
addCommonStyle = function(){
  let sheetData = this.sheetData;
  let trStyle = (()=>{
    let echo = '';
    for(let key in sheetData.cols){
      if(sheetData.cols[key].width){
        echo +=  `.${cssPrefix}-preview-tr 
        .${cssPrefix}-preview-td:nth-child(${+key+1}){
          width: ${sheetData.cols[key].width}px;
          max-width: ${sheetData.cols[key].width}px
        }\n`
      }
    }
    for(let key in sheetData.rows){
      if(sheetData.rows[key].height){
        echo +=  `.${cssPrefix}-preview-tr:nth-child(${+key+1}) 
        .${cssPrefix}-preview-td{
          height: ${sheetData.rows[key].height}px;
          max-height: ${sheetData.rows[key].height}px
        }\n`
      }
    }
    return echo;
  })();
  this.styleEl.html(`
    .${cssPrefix}-preview-td{
      width: ${this.data.cols.width}px;
      height: ${this.data.rows.height}px;
      max-width: ${this.data.cols.width}px;
      max-height: ${this.data.rows.height}px;
      font-size: ${this.data.settings.style.font.size}px;
      font-family: ${this.data.settings.style.font.name};
      color: ${this.data.settings.style.color};
      text-align: ${this.data.settings.style.align};
      vertical-align: ${this.data.settings.style.valign};
    }\n
    ${trStyle}
  `)
};
export default class Preview{
  constructor(data){
    this.isHide = true;
    this.data = data;
    this.sheetData = this.data.getData();
    this.el = h('div', `${cssPrefix}-preview`)
      .children(
        this.styleEl = h('style'),
        h('div',`${cssPrefix}-preview-bar`).children(
          h('div', '-title').child(t('preview.title')),
          h('div', '-right').children(
            h('div', `${cssPrefix}-buttons`).children(
              new Button('close').on('click', btnClick.bind(this, 'close')),
              new Button('export', 'primary').on('click', btnClick.bind(this, 'export')),
            ),
          ),
        ),
        this.contentEl = h('div',`${cssPrefix}-preview-content`).children(
          this.tableEl = h('table',`${cssPrefix}-preview-table`).attr({
            cellspacing: 0,
            border: 1
          })
        )
      ).hide();
  }
  async reset(){
    if (this.isHide) return;
    this.tableEl.html('').hide();
    this.sheetData = this.data.getData();
    let {cellsMap,rowLen,colLen} = await getTable(this.sheetData,this.data.dataSource || []);
    //设置样式
    addCommonStyle.call(this);
    //添加cell
    for(let ri = 0; ri <= rowLen; ri++){
      let trDom = addTrDom();
      for(let ci = 0; ci <= colLen; ci++){
        let cell = cellsMap.get(`${ri}_${ci}`);
        let tdDom = addTdDom();
        if(cell && cell.hide)continue;
        setText.call(this,tdDom,cell);
        setStyle.call(this,tdDom,cell);
        setMerge.call(this,tdDom,cell);
        setConditionStyle.call(this,tdDom,cell);
        trDom.child(tdDom);
      }
      this.tableEl.child(trDom)
    }
    this.tableEl.show();
  }
  resetData(data) {
    this.data = data;
    this.reset();
  }
  export(){
    let { name } = this.data.getData();
    exportExcel(name,this.tableEl.el,this.styleEl.html());
  }
  open(){
    this.isHide = false;
    this.reset();
    this.el.show();
  }
}