/* eslint-disable no-param-reassign */
function cloneDeep(obj){
  var objClone = obj instanceof Array ? []:{}
  for(let key in obj){
    if( typeof obj[key]==='object'){
      //1:引用类型的属性也是一个引用类型的时候
      objClone[key] = cloneDeep(obj[key]);
    }else{
      objClone[key] = obj[key];
    }
  };
  return objClone
}
const mergeDeep = (object = {}, ...sources) => {
  sources.forEach((source) => {
    Object.keys(source).forEach((key) => {
      const v = source[key];
      // console.log('k:', key, ', v:', source[key], typeof v, v instanceof Object);
      if (typeof v === 'string' || typeof v === 'number' || typeof v === 'boolean') {
        object[key] = v;
      } else if (typeof v !== 'function' && !Array.isArray(v) && v instanceof Object) {
        object[key] = object[key] || {};
        mergeDeep(object[key], v);
      } else {
        object[key] = v;
      }
    });
  });
  // console.log('::', object);
  return object;
};

function equals(obj1, obj2) {
  const keys = Object.keys(obj1);
  if (keys.length !== Object.keys(obj2).length) return false;
  for (let i = 0; i < keys.length; i += 1) {
    const k = keys[i];
    const v1 = obj1[k];
    const v2 = obj2[k];
    if (v2 === undefined) return false;
    if (typeof v1 === 'string' || typeof v1 === 'number' || typeof v1 === 'boolean') {
      if (v1 !== v2) return false;
    } else if (Array.isArray(v1)) {
      if (v1.length !== v2.length) return false;
      for (let ai = 0; ai < v1.length; ai += 1) {
        if (!equals(v1[ai], v2[ai])) return false;
      }
    } else if (typeof v1 !== 'function' && !Array.isArray(v1) && v1 instanceof Object) {
      if (!equals(v1, v2)) return false;
    }
  }
  return true;
}

/*
  objOrAry: obejct or Array
  cb: (value, index | key) => { return value }
*/
const sum = (objOrAry, cb = value => value) => {
  let total = 0;
  let size = 0;
  Object.keys(objOrAry).forEach((key) => {
    total += cb(objOrAry[key], key);
    size += 1;
  });
  return [total, size];
};

function deleteProperty(obj, property) {
  const oldv = obj[`${property}`];
  delete obj[`${property}`];
  return oldv;
}

function rangeReduceIf(min, max, inits, initv, ifv, getv) {
  let s = inits;
  let v = initv;
  let i = min;
  for (; i < max; i += 1) {
    if (s > ifv) break;
    v = getv(i);
    s += v;
  }
  return [i, s - v, v];
}

function rangeSum(min, max, getv) {
  let s = 0;
  for (let i = min; i < max; i += 1) {
    s += getv(i);
  }
  return s;
}

function rangeEach(min, max, cb) {
  for (let i = min; i < max; i += 1) {
    cb(i);
  }
}

function arrayEquals(a1, a2) {
  if (a1.length === a2.length) {
    for (let i = 0; i < a1.length; i += 1) {
      if (a1[i] !== a2[i]) return false;
    }
  } else return false;
  return true;
}

function digits(a) {
  const v = `${a}`;
  let ret = 0;
  let flag = false;
  for (let i = 0; i < v.length; i += 1) {
    if (flag === true) ret += 1;
    if (v.charAt(i) === '.') flag = true;
  }
  return ret;
}

export function numberCalc(type, a1, a2) {
  if (Number.isNaN(a1) || Number.isNaN(a2)) {
    return a1 + type + a2;
  }
  const al1 = digits(a1);
  const al2 = digits(a2);
  const num1 = Number(a1);
  const num2 = Number(a2);
  let ret = 0;
  if (type === '-') {
    ret = num1 - num2;
  } else if (type === '+') {
    ret = num1 + num2;
  } else if (type === '*') {
    ret = num1 * num2;
  } else if (type === '/') {
    ret = num1 / num2;
    if (digits(ret) > 5) return ret.toFixed(2);
    return ret;
  }
  return ret.toFixed(Math.max(al1, al2));
}

/**
 * @description 防抖函数
 */
 export const debounce = function (func, wait=50) {
  let timer;
  let echo = function(...result) {
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      func.call(echo.prototype.target,...result)
    }, wait)
  };
  echo.prototype.target = window;
  echo.bind = function(target){
    echo.prototype.target = target;
    return echo;
  }
  return echo
}

export const throttle = function(delay, callback) {
  let timeoutID;
  let lastExec = 0;
  function wrapper() {
    const elapsed = Number(new Date()) - lastExec;
    const args = arguments;
    function exec() {
      lastExec = Number(new Date());
      callback(args);
    }
    clearTimeout(timeoutID);
    if (elapsed > delay) {
      exec();
    } else {
      timeoutID = setTimeout(exec, delay - elapsed);
    }
  }
  return wrapper;
};

const toString = Object.prototype.toString

export function is (val, type) {
  return toString.call(val) === `[object ${type}]`
}

export function isDef (val){
  return typeof val !== 'undefined'
}

export function isUnDef(val){
  return !isDef(val)
}

export function isObject (val) {
  return val !== null && is(val, 'Object')
}

export function isEmpty(val){
  if (isArray(val) || isString(val)) {
    return val.length === 0
  }

  if (val instanceof Map || val instanceof Set) {
    return val.size === 0
  }

  if (isObject(val)) {
    return Object.keys(val).length === 0
  }

  return false
}

export function isDate (val){
  return is(val, 'Date')
}

export function isNull (val){
  return val === null
}

export function isNullAndUnDef (val){
  return isUnDef(val) && isNull(val)
}

export function isNullOrUnDef (val){
  return isUnDef(val) || isNull(val)
}

export function isNumber (val){
  return is(val, 'Number')
}

export function isPromise(val){
  return is(val, 'Promise') && isObject(val) && isFunction(val.then) && isFunction(val.catch)
}

export function isString (val){
  return is(val, 'String')
}

export function isFunction (val){
  return typeof val === 'function'
}

export function isBoolean (val){
  return is(val, 'Boolean')
}

export function isRegExp (val){
  return is(val, 'RegExp')
}

export function isArray (val){
  return val && Array.isArray(val)
}

export function isWindow (val){
  return typeof window !== 'undefined' && is(val, 'Window')
}

export function isElement (val) {
  return isObject(val) && !!val.tagName
}

export const isServer = typeof window === 'undefined'

export const isClient = !isServer

export function isUrl (path) {
  const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/
  return reg.test(path)
}

export default {
  cloneDeep,
  merge: (...sources) => mergeDeep({}, ...sources),
  equals,
  arrayEquals,
  sum,
  rangeEach,
  rangeSum,
  rangeReduceIf,
  deleteProperty,
  numberCalc,
};
